extends MarginContainer

onready var fps_label = get_node('VBoxContainer/FPS')

func _process(delta):
	fps_label.set_text("FPS: " + str(Engine.get_frames_per_second()))


func _on_toggleFullscreen():
	OS.window_fullscreen = !OS.window_fullscreen
